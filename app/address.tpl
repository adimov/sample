<div class="step3 hide two x-ac x-cover">
                            <p><span></span>Погрузка и выгрузка</p>

                            <div class="city">
                                <p>Город (откуда) <span>*</span></p>
                                <div class="inquiry_block">
                                    <a data-field-id="x-additional-origin-place" href="javascript:;" class="add_a_city x-add-place" title="Добавить">+</a>
                            </div>
<div class="city_DropDown">
    <select class="x-msdd x-ac-related"
            name="location[0][1][country]"
            data-ac-related-id="1927699170"
            data-ac-related-name="country_id">
                            <option value="26"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag au">
                        Австралия                                            ( Australia )
                            </option>
                    <option value="39"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag at">
                        Австрия                                            ( Österreich )
                            </option>
                    <option value="19"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag az">
                        Азербайджан                                            ( Azərbaycan )
                            </option>
                    <option value="27"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ar">
                        Аргентина                                            ( Argentina )
                            </option>
                    <option value="57"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag am">
                        Армения                                            ( Հայաստան )
                            </option>
                    <option value="78"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag bz">
                        Белиз                                            ( Belize )
                            </option>
                    <option value="10"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag by">
                        Белоруссия                                            ( Беларусь )
                            </option>
                    <option value="44"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag be">
                        Бельгия                                            ( Belgien )
                            </option>
                    <option value="29"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag bg">
                        Болгария                                            ( България )
                            </option>
                    <option value="13"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag br">
                        Бразилия                                            ( Brasil )
                            </option>
                    <option value="4"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag gb">
                        Великобритания                                            ( United Kingdom )
                            </option>
                    <option value="34"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag hu">
                        Венгрия                                            ( Magyarország )
                            </option>
                    <option value="46"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag vn">
                        Вьетнам                                            ( Việt Nam )
                            </option>
                    <option value="56"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ht">
                        Гаити                                            ( Haiti )
                            </option>
                    <option value="8"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag de">
                        Германия                                            ( Deutschland )
                            </option>
                    <option value="80"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag hn">
                        Гондурас                                            ( Honduras )
                            </option>
                    <option value="48"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag gr">
                        Греция                                            ( Ελλάδα )
                            </option>
                    <option value="20"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ge">
                        Грузия                                            ( საქართველოს )
                            </option>
                    <option value="52"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag dk">
                        Дания                                            ( Danmark )
                            </option>
                    <option value="51"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag eg">
                        Египет                                            ( مصر )
                            </option>
                    <option value="24"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag il">
                        Израиль                                            ( ישראל )
                            </option>
                    <option value="9"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag in">
                        Индия                                            ( भारत )
                            </option>
                    <option value="99"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag id">
                        Индонезия                                            ( Indonesia )
                            </option>
                    <option value="98"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag jo">
                        Иордания                                            ( الأردن )
                            </option>
                    <option value="65"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag iq">
                        Ирак                                            ( العراق )
                            </option>
                    <option value="45"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ir">
                        Иран                                            ( ایران )
                            </option>
                    <option value="58"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ie">
                        Ирландия                                            ( Éire )
                            </option>
                    <option value="22"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag es">
                        Испания                                            ( España )
                            </option>
                    <option value="16"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag it">
                        Италия                                            ( Italia )
                            </option>
                    <option value="5"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag kz">
                        Казахстан                                            ( Қазақстан )
                            </option>
                    <option value="61"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cm">
                        Камерун                                            ( Cameroon )
                            </option>
                    <option value="14"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ca">
                        Канада                                            ( Canada )
                            </option>
                    <option value="60"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cy">
                        Кипр                                            ( Κύπρος )
                            </option>
                    <option value="18"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag kg">
                        Киргизия                                            ( Kyrgyzstan )
                            </option>
                    <option value="30"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cn">
                        Китай                                            ( 中國 )
                            </option>
                    <option value="59"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cr">
                        Коста-Рика                                            ( Costa Rica )
                            </option>
                    <option value="96"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cu">
                        Куба                                            ( Cuba )
                            </option>
                    <option value="75"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag kw">
                        Кувейт                                            ( الكويت )
                            </option>
                    <option value="23"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag lv">
                        Латвия                                            ( Latvija )
                            </option>
                    <option value="76"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag lb">
                        Ливан                                            ( لبنان )
                            </option>
                    <option value="72"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ly">
                        Ливия                                            ( ليبيا )
                            </option>
                    <option value="17"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag lt">
                        Литва                                            ( Lietuva )
                            </option>
                    <option value="83"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag lu">
                        Люксембург                                            ( Luxembourg )
                            </option>
                    <option value="100"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag mk">
                        Македония                                            ( Македонија )
                            </option>
                    <option value="101"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag my">
                        Малайзия                                            ( Malaysia )
                            </option>
                    <option value="105"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag mt">
                        Мальта                                            ( Malta )
                            </option>
                    <option value="70"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ma">
                        Марокко                                            ( مغربي )
                            </option>
                    <option value="11"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag mx">
                        Мексика                                            ( México )
                            </option>
                    <option value="63"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag mz">
                        Мозамбик                                            ( Moçambique )
                            </option>
                    <option value="28"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag md">
                        Молдавия                                            ( Republica Moldova )
                            </option>
                    <option value="73"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag mc">
                        Монако                                            ( Monaco )
                            </option>
                    <option value="104"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag mn">
                        Монголия                                            ( Mongolia )
                            </option>
                    <option value="37"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag nz">
                        Новая Зеландия                                            ( New Zealand )
                            </option>
                    <option value="47"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag no">
                        Норвегия                                            ( Norge )
                            </option>
                    <option value="66"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag pk">
                        Пакистан                                            ( Pakistan )
                            </option>
                    <option value="102"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag pe">
                        Перу                                            ( Perú )
                            </option>
                    <option value="7"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag pl">
                        Польша                                            ( Polska )
                            </option>
                    <option value="49"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag pt">
                        Португалия                                            ( Portugal )
                            </option>
                    <option value="1"
            selected="selected"                    data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ru">
                        Россия                                            ( Russia )
                            </option>
                    <option value="38"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ro">
                        Румыния                                            ( România )
                            </option>
                    <option value="50"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag sv">
                        Сальвадор                                            ( Salvador )
                            </option>
                    <option value="94"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag sg">
                        Сингапур                                            ( Singapore )
                            </option>
                    <option value="87"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag sy">
                        Сирия                                            ( سوريا )
                            </option>
                    <option value="67"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag sk">
                        Словакия                                            ( Slovensko )
                            </option>
                    <option value="62"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag si">
                        Словения                                            ( Slovenija )
                            </option>
                    <option value="86"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag sr">
                        Суринам                                            ( Suriname )
                            </option>
                    <option value="2"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag us">
                        США                                            ( USA )
                            </option>
                    <option value="21"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag tj">
                        Таджикистан                                            ( Tajikistan )
                            </option>
                    <option value="88"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag th">
                        Таиланд                                            ( ประเทศไทย )
                            </option>
                    <option value="89"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag tn">
                        Тунис                                            ( تونس )
                            </option>
                    <option value="64"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag tm">
                        Туркмения                                            ( Turkmenistan )
                            </option>
                    <option value="32"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag tr">
                        Турция                                            ( Türkiye )
                            </option>
                    <option value="69"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ug">
                        Уганда                                            ( Uganda )
                            </option>
                    <option value="15"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag uz">
                        Узбекистан                                            ( Ўзбекистон )
                            </option>
                    <option value="3"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ua">
                        Украина                                            ( Україна )
                            </option>
                    <option value="41"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag fi">
                        Финляндия                                            ( Suomi )
                            </option>
                    <option value="12"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag fr">
                        Франция                                            ( France )
                            </option>
                    <option value="53"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag hr">
                        Хорватия                                            ( Hrvatska )
                            </option>
                    <option value="107"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag me">
                        Черногория                                            ( Црна Гора )
                            </option>
                    <option value="40"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cz">
                        Чехия                                            ( Česká republika )
                            </option>
                    <option value="93"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cl">
                        Чили                                            ( Chile )
                            </option>
                    <option value="43"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ch">
                        Швейцария                                            ( Schweiz )
                            </option>
                    <option value="36"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag se">
                        Швеция                                            ( Sverige )
                            </option>
                    <option value="92"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ec">
                        Эквадор                                            ( Ecuador )
                            </option>
                    <option value="33"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ee">
                        Эстония                                            ( Eesti )
                            </option>
                    <option value="35"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag kp">
                        Южная Корея                                            ( 대한민국 )
                            </option>
                    <option value="90"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag jm">
                        Ямайка                                            ( Jamaica )
                            </option>
                    <option value="6"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag jp">
                        Япония                                            ( 日本 )
                            </option>
            </select>
</div>
<span class="x-required city-input">
    <input type="text"
            class="text1 x-ac-input x-ac-city x-ac-related x-open-up x-from-city"
            placeholder="Используйте автоподбор"
            name="location[0][1][city]"
            value=""
            data-ac-item-id="175194"
            data-ac-related="1927699170"
            data-ac-related-id="968460709"
            data-ac-related-name="city_id"
            data-open-id="3">
</span>
<input type="text"
       class="text2 x-ac-input x-ac-street x-ac-related x-from-street"
       placeholder="Улица"
       data-ac-related="968460709" name="location[0][1][street]">
<input type="text"
       class="text3"
       placeholder="Дом"
       name="location[0][1][home]">                            </div>
                            <div id="x-additional-origin-place" class="city hide">
                                <p>Второй город (откуда)</p>
                                <div class="inquiry_block">
            <a data-field-id="x-additional-origin-place" href="javascript:;" class="add_a_city x-remove-place dop15" title="Убрать">_</a>
    </div>
<div class="city_DropDown">
    <select class="x-msdd x-ac-related"
            name="location[1][1][country]"
            data-ac-related-id="643953794"
            data-ac-related-name="country_id">
                            <option value="26"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag au">
                        Австралия                                            ( Australia )
                            </option>
                    <option value="39"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag at">
                        Австрия                                            ( Österreich )
                            </option>
                    <option value="19"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag az">
                        Азербайджан                                            ( Azərbaycan )
                            </option>
                    <option value="27"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ar">
                        Аргентина                                            ( Argentina )
                            </option>
                    <option value="57"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag am">
                        Армения                                            ( Հայաստան )
                            </option>
                    <option value="78"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag bz">
                        Белиз                                            ( Belize )
                            </option>
                    <option value="10"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag by">
                        Белоруссия                                            ( Беларусь )
                            </option>
                    <option value="44"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag be">
                        Бельгия                                            ( Belgien )
                            </option>
                    <option value="29"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag bg">
                        Болгария                                            ( България )
                            </option>
                    <option value="13"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag br">
                        Бразилия                                            ( Brasil )
                            </option>
                    <option value="4"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag gb">
                        Великобритания                                            ( United Kingdom )
                            </option>
                    <option value="34"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag hu">
                        Венгрия                                            ( Magyarország )
                            </option>
                    <option value="46"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag vn">
                        Вьетнам                                            ( Việt Nam )
                            </option>
                    <option value="56"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ht">
                        Гаити                                            ( Haiti )
                            </option>
                    <option value="8"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag de">
                        Германия                                            ( Deutschland )
                            </option>
                    <option value="80"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag hn">
                        Гондурас                                            ( Honduras )
                            </option>
                    <option value="48"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag gr">
                        Греция                                            ( Ελλάδα )
                            </option>
                    <option value="20"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ge">
                        Грузия                                            ( საქართველოს )
                            </option>
                    <option value="52"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag dk">
                        Дания                                            ( Danmark )
                            </option>
                    <option value="51"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag eg">
                        Египет                                            ( مصر )
                            </option>
                    <option value="24"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag il">
                        Израиль                                            ( ישראל )
                            </option>
                    <option value="9"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag in">
                        Индия                                            ( भारत )
                            </option>
                    <option value="99"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag id">
                        Индонезия                                            ( Indonesia )
                            </option>
                    <option value="98"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag jo">
                        Иордания                                            ( الأردن )
                            </option>
                    <option value="65"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag iq">
                        Ирак                                            ( العراق )
                            </option>
                    <option value="45"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ir">
                        Иран                                            ( ایران )
                            </option>
                    <option value="58"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ie">
                        Ирландия                                            ( Éire )
                            </option>
                    <option value="22"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag es">
                        Испания                                            ( España )
                            </option>
                    <option value="16"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag it">
                        Италия                                            ( Italia )
                            </option>
                    <option value="5"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag kz">
                        Казахстан                                            ( Қазақстан )
                            </option>
                    <option value="61"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cm">
                        Камерун                                            ( Cameroon )
                            </option>
                    <option value="14"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ca">
                        Канада                                            ( Canada )
                            </option>
                    <option value="60"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cy">
                        Кипр                                            ( Κύπρος )
                            </option>
                    <option value="18"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag kg">
                        Киргизия                                            ( Kyrgyzstan )
                            </option>
                    <option value="30"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cn">
                        Китай                                            ( 中國 )
                            </option>
                    <option value="59"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cr">
                        Коста-Рика                                            ( Costa Rica )
                            </option>
                    <option value="96"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cu">
                        Куба                                            ( Cuba )
                            </option>
                    <option value="75"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag kw">
                        Кувейт                                            ( الكويت )
                            </option>
                    <option value="23"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag lv">
                        Латвия                                            ( Latvija )
                            </option>
                    <option value="76"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag lb">
                        Ливан                                            ( لبنان )
                            </option>
                    <option value="72"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ly">
                        Ливия                                            ( ليبيا )
                            </option>
                    <option value="17"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag lt">
                        Литва                                            ( Lietuva )
                            </option>
                    <option value="83"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag lu">
                        Люксембург                                            ( Luxembourg )
                            </option>
                    <option value="100"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag mk">
                        Македония                                            ( Македонија )
                            </option>
                    <option value="101"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag my">
                        Малайзия                                            ( Malaysia )
                            </option>
                    <option value="105"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag mt">
                        Мальта                                            ( Malta )
                            </option>
                    <option value="70"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ma">
                        Марокко                                            ( مغربي )
                            </option>
                    <option value="11"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag mx">
                        Мексика                                            ( México )
                            </option>
                    <option value="63"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag mz">
                        Мозамбик                                            ( Moçambique )
                            </option>
                    <option value="28"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag md">
                        Молдавия                                            ( Republica Moldova )
                            </option>
                    <option value="73"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag mc">
                        Монако                                            ( Monaco )
                            </option>
                    <option value="104"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag mn">
                        Монголия                                            ( Mongolia )
                            </option>
                    <option value="37"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag nz">
                        Новая Зеландия                                            ( New Zealand )
                            </option>
                    <option value="47"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag no">
                        Норвегия                                            ( Norge )
                            </option>
                    <option value="66"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag pk">
                        Пакистан                                            ( Pakistan )
                            </option>
                    <option value="102"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag pe">
                        Перу                                            ( Perú )
                            </option>
                    <option value="7"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag pl">
                        Польша                                            ( Polska )
                            </option>
                    <option value="49"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag pt">
                        Португалия                                            ( Portugal )
                            </option>
                    <option value="1"
            selected="selected"                    data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ru">
                        Россия                                            ( Russia )
                            </option>
                    <option value="38"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ro">
                        Румыния                                            ( România )
                            </option>
                    <option value="50"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag sv">
                        Сальвадор                                            ( Salvador )
                            </option>
                    <option value="94"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag sg">
                        Сингапур                                            ( Singapore )
                            </option>
                    <option value="87"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag sy">
                        Сирия                                            ( سوريا )
                            </option>
                    <option value="67"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag sk">
                        Словакия                                            ( Slovensko )
                            </option>
                    <option value="62"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag si">
                        Словения                                            ( Slovenija )
                            </option>
                    <option value="86"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag sr">
                        Суринам                                            ( Suriname )
                            </option>
                    <option value="2"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag us">
                        США                                            ( USA )
                            </option>
                    <option value="21"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag tj">
                        Таджикистан                                            ( Tajikistan )
                            </option>
                    <option value="88"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag th">
                        Таиланд                                            ( ประเทศไทย )
                            </option>
                    <option value="89"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag tn">
                        Тунис                                            ( تونس )
                            </option>
                    <option value="64"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag tm">
                        Туркмения                                            ( Turkmenistan )
                            </option>
                    <option value="32"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag tr">
                        Турция                                            ( Türkiye )
                            </option>
                    <option value="69"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ug">
                        Уганда                                            ( Uganda )
                            </option>
                    <option value="15"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag uz">
                        Узбекистан                                            ( Ўзбекистон )
                            </option>
                    <option value="3"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ua">
                        Украина                                            ( Україна )
                            </option>
                    <option value="41"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag fi">
                        Финляндия                                            ( Suomi )
                            </option>
                    <option value="12"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag fr">
                        Франция                                            ( France )
                            </option>
                    <option value="53"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag hr">
                        Хорватия                                            ( Hrvatska )
                            </option>
                    <option value="107"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag me">
                        Черногория                                            ( Црна Гора )
                            </option>
                    <option value="40"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cz">
                        Чехия                                            ( Česká republika )
                            </option>
                    <option value="93"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cl">
                        Чили                                            ( Chile )
                            </option>
                    <option value="43"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ch">
                        Швейцария                                            ( Schweiz )
                            </option>
                    <option value="36"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag se">
                        Швеция                                            ( Sverige )
                            </option>
                    <option value="92"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ec">
                        Эквадор                                            ( Ecuador )
                            </option>
                    <option value="33"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ee">
                        Эстония                                            ( Eesti )
                            </option>
                    <option value="35"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag kp">
                        Южная Корея                                            ( 대한민국 )
                            </option>
                    <option value="90"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag jm">
                        Ямайка                                            ( Jamaica )
                            </option>
                    <option value="6"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag jp">
                        Япония                                            ( 日本 )
                            </option>
            </select>
</div>
<span class="x-required city-input">
    <input type="text"
            class="text1 x-ac-input x-ac-city x-ac-related x-open-up "
            placeholder="Используйте автоподбор"
            name="location[1][1][city]"
            value=""
            data-ac-item-id=""
            data-ac-related="643953794"
            data-ac-related-id="446560914"
            data-ac-related-name="city_id"
            data-open-id="3">
</span>
<input type="text"
       class="text2 x-ac-input x-ac-street x-ac-related "
       placeholder="Улица"
       data-ac-related="446560914" name="location[1][1][street]">
<input type="text"
       class="text3"
       placeholder="Дом"
       name="location[1][1][home]">                            </div>
                            <div class="city mt30">
                                <p>Город (куда) <span>*</span></p>
                                <div class="inquiry_block">
            <a data-field-id="x-additional-destination-place" href="javascript:;" class="add_a_city x-add-place" title="Добавить">+</a>
    </div>
<div class="city_DropDown">
    <select class="x-msdd x-ac-related"
            name="location[0][2][country]"
            data-ac-related-id="238034582"
            data-ac-related-name="country_id">
                            <option value="26"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag au">
                        Австралия                                            ( Australia )
                            </option>
                    <option value="39"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag at">
                        Австрия                                            ( Österreich )
                            </option>
                    <option value="19"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag az">
                        Азербайджан                                            ( Azərbaycan )
                            </option>
                    <option value="27"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ar">
                        Аргентина                                            ( Argentina )
                            </option>
                    <option value="57"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag am">
                        Армения                                            ( Հայաստան )
                            </option>
                    <option value="78"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag bz">
                        Белиз                                            ( Belize )
                            </option>
                    <option value="10"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag by">
                        Белоруссия                                            ( Беларусь )
                            </option>
                    <option value="44"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag be">
                        Бельгия                                            ( Belgien )
                            </option>
                    <option value="29"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag bg">
                        Болгария                                            ( България )
                            </option>
                    <option value="13"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag br">
                        Бразилия                                            ( Brasil )
                            </option>
                    <option value="4"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag gb">
                        Великобритания                                            ( United Kingdom )
                            </option>
                    <option value="34"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag hu">
                        Венгрия                                            ( Magyarország )
                            </option>
                    <option value="46"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag vn">
                        Вьетнам                                            ( Việt Nam )
                            </option>
                    <option value="56"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ht">
                        Гаити                                            ( Haiti )
                            </option>
                    <option value="8"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag de">
                        Германия                                            ( Deutschland )
                            </option>
                    <option value="80"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag hn">
                        Гондурас                                            ( Honduras )
                            </option>
                    <option value="48"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag gr">
                        Греция                                            ( Ελλάδα )
                            </option>
                    <option value="20"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ge">
                        Грузия                                            ( საქართველოს )
                            </option>
                    <option value="52"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag dk">
                        Дания                                            ( Danmark )
                            </option>
                    <option value="51"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag eg">
                        Египет                                            ( مصر )
                            </option>
                    <option value="24"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag il">
                        Израиль                                            ( ישראל )
                            </option>
                    <option value="9"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag in">
                        Индия                                            ( भारत )
                            </option>
                    <option value="99"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag id">
                        Индонезия                                            ( Indonesia )
                            </option>
                    <option value="98"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag jo">
                        Иордания                                            ( الأردن )
                            </option>
                    <option value="65"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag iq">
                        Ирак                                            ( العراق )
                            </option>
                    <option value="45"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ir">
                        Иран                                            ( ایران )
                            </option>
                    <option value="58"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ie">
                        Ирландия                                            ( Éire )
                            </option>
                    <option value="22"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag es">
                        Испания                                            ( España )
                            </option>
                    <option value="16"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag it">
                        Италия                                            ( Italia )
                            </option>
                    <option value="5"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag kz">
                        Казахстан                                            ( Қазақстан )
                            </option>
                    <option value="61"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cm">
                        Камерун                                            ( Cameroon )
                            </option>
                    <option value="14"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ca">
                        Канада                                            ( Canada )
                            </option>
                    <option value="60"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cy">
                        Кипр                                            ( Κύπρος )
                            </option>
                    <option value="18"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag kg">
                        Киргизия                                            ( Kyrgyzstan )
                            </option>
                    <option value="30"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cn">
                        Китай                                            ( 中國 )
                            </option>
                    <option value="59"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cr">
                        Коста-Рика                                            ( Costa Rica )
                            </option>
                    <option value="96"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cu">
                        Куба                                            ( Cuba )
                            </option>
                    <option value="75"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag kw">
                        Кувейт                                            ( الكويت )
                            </option>
                    <option value="23"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag lv">
                        Латвия                                            ( Latvija )
                            </option>
                    <option value="76"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag lb">
                        Ливан                                            ( لبنان )
                            </option>
                    <option value="72"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ly">
                        Ливия                                            ( ليبيا )
                            </option>
                    <option value="17"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag lt">
                        Литва                                            ( Lietuva )
                            </option>
                    <option value="83"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag lu">
                        Люксембург                                            ( Luxembourg )
                            </option>
                    <option value="100"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag mk">
                        Македония                                            ( Македонија )
                            </option>
                    <option value="101"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag my">
                        Малайзия                                            ( Malaysia )
                            </option>
                    <option value="105"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag mt">
                        Мальта                                            ( Malta )
                            </option>
                    <option value="70"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ma">
                        Марокко                                            ( مغربي )
                            </option>
                    <option value="11"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag mx">
                        Мексика                                            ( México )
                            </option>
                    <option value="63"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag mz">
                        Мозамбик                                            ( Moçambique )
                            </option>
                    <option value="28"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag md">
                        Молдавия                                            ( Republica Moldova )
                            </option>
                    <option value="73"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag mc">
                        Монако                                            ( Monaco )
                            </option>
                    <option value="104"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag mn">
                        Монголия                                            ( Mongolia )
                            </option>
                    <option value="37"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag nz">
                        Новая Зеландия                                            ( New Zealand )
                            </option>
                    <option value="47"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag no">
                        Норвегия                                            ( Norge )
                            </option>
                    <option value="66"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag pk">
                        Пакистан                                            ( Pakistan )
                            </option>
                    <option value="102"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag pe">
                        Перу                                            ( Perú )
                            </option>
                    <option value="7"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag pl">
                        Польша                                            ( Polska )
                            </option>
                    <option value="49"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag pt">
                        Португалия                                            ( Portugal )
                            </option>
                    <option value="1"
            selected="selected"                    data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ru">
                        Россия                                            ( Russia )
                            </option>
                    <option value="38"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ro">
                        Румыния                                            ( România )
                            </option>
                    <option value="50"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag sv">
                        Сальвадор                                            ( Salvador )
                            </option>
                    <option value="94"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag sg">
                        Сингапур                                            ( Singapore )
                            </option>
                    <option value="87"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag sy">
                        Сирия                                            ( سوريا )
                            </option>
                    <option value="67"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag sk">
                        Словакия                                            ( Slovensko )
                            </option>
                    <option value="62"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag si">
                        Словения                                            ( Slovenija )
                            </option>
                    <option value="86"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag sr">
                        Суринам                                            ( Suriname )
                            </option>
                    <option value="2"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag us">
                        США                                            ( USA )
                            </option>
                    <option value="21"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag tj">
                        Таджикистан                                            ( Tajikistan )
                            </option>
                    <option value="88"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag th">
                        Таиланд                                            ( ประเทศไทย )
                            </option>
                    <option value="89"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag tn">
                        Тунис                                            ( تونس )
                            </option>
                    <option value="64"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag tm">
                        Туркмения                                            ( Turkmenistan )
                            </option>
                    <option value="32"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag tr">
                        Турция                                            ( Türkiye )
                            </option>
                    <option value="69"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ug">
                        Уганда                                            ( Uganda )
                            </option>
                    <option value="15"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag uz">
                        Узбекистан                                            ( Ўзбекистон )
                            </option>
                    <option value="3"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ua">
                        Украина                                            ( Україна )
                            </option>
                    <option value="41"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag fi">
                        Финляндия                                            ( Suomi )
                            </option>
                    <option value="12"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag fr">
                        Франция                                            ( France )
                            </option>
                    <option value="53"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag hr">
                        Хорватия                                            ( Hrvatska )
                            </option>
                    <option value="107"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag me">
                        Черногория                                            ( Црна Гора )
                            </option>
                    <option value="40"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cz">
                        Чехия                                            ( Česká republika )
                            </option>
                    <option value="93"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cl">
                        Чили                                            ( Chile )
                            </option>
                    <option value="43"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ch">
                        Швейцария                                            ( Schweiz )
                            </option>
                    <option value="36"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag se">
                        Швеция                                            ( Sverige )
                            </option>
                    <option value="92"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ec">
                        Эквадор                                            ( Ecuador )
                            </option>
                    <option value="33"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ee">
                        Эстония                                            ( Eesti )
                            </option>
                    <option value="35"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag kp">
                        Южная Корея                                            ( 대한민국 )
                            </option>
                    <option value="90"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag jm">
                        Ямайка                                            ( Jamaica )
                            </option>
                    <option value="6"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag jp">
                        Япония                                            ( 日本 )
                            </option>
            </select>
</div>
<span class="x-required city-input">
    <input type="text"
            class="text1 x-ac-input x-ac-city x-ac-related x-open-up "
            placeholder="Используйте автоподбор"
            name="location[0][2][city]"
            value=""
            data-ac-item-id=""
            data-ac-related="238034582"
            data-ac-related-id="1080759246"
            data-ac-related-name="city_id"
            data-open-id="3">
</span>
<input type="text"
       class="text2 x-ac-input x-ac-street x-ac-related "
       placeholder="Улица"
       data-ac-related="1080759246" name="location[0][2][street]">
<input type="text"
       class="text3"
       placeholder="Дом"
       name="location[0][2][home]">                            </div>
                            <div id="x-additional-destination-place" class="city hide">
                                <p>Второй город (куда)</p>
                                <div class="inquiry_block">
            <a data-field-id="x-additional-destination-place" href="javascript:;" class="add_a_city x-remove-place dop15" title="Убрать">_</a>
    </div>
<div class="city_DropDown">
    <select class="x-msdd x-ac-related"
            name="location[1][2][country]"
            data-ac-related-id="460471187"
            data-ac-related-name="country_id">
                            <option value="26"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag au">
                        Австралия                                            ( Australia )
                            </option>
                    <option value="39"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag at">
                        Австрия                                            ( Österreich )
                            </option>
                    <option value="19"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag az">
                        Азербайджан                                            ( Azərbaycan )
                            </option>
                    <option value="27"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ar">
                        Аргентина                                            ( Argentina )
                            </option>
                    <option value="57"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag am">
                        Армения                                            ( Հայաստան )
                            </option>
                    <option value="78"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag bz">
                        Белиз                                            ( Belize )
                            </option>
                    <option value="10"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag by">
                        Белоруссия                                            ( Беларусь )
                            </option>
                    <option value="44"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag be">
                        Бельгия                                            ( Belgien )
                            </option>
                    <option value="29"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag bg">
                        Болгария                                            ( България )
                            </option>
                    <option value="13"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag br">
                        Бразилия                                            ( Brasil )
                            </option>
                    <option value="4"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag gb">
                        Великобритания                                            ( United Kingdom )
                            </option>
                    <option value="34"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag hu">
                        Венгрия                                            ( Magyarország )
                            </option>
                    <option value="46"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag vn">
                        Вьетнам                                            ( Việt Nam )
                            </option>
                    <option value="56"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ht">
                        Гаити                                            ( Haiti )
                            </option>
                    <option value="8"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag de">
                        Германия                                            ( Deutschland )
                            </option>
                    <option value="80"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag hn">
                        Гондурас                                            ( Honduras )
                            </option>
                    <option value="48"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag gr">
                        Греция                                            ( Ελλάδα )
                            </option>
                    <option value="20"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ge">
                        Грузия                                            ( საქართველოს )
                            </option>
                    <option value="52"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag dk">
                        Дания                                            ( Danmark )
                            </option>
                    <option value="51"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag eg">
                        Египет                                            ( مصر )
                            </option>
                    <option value="24"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag il">
                        Израиль                                            ( ישראל )
                            </option>
                    <option value="9"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag in">
                        Индия                                            ( भारत )
                            </option>
                    <option value="99"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag id">
                        Индонезия                                            ( Indonesia )
                            </option>
                    <option value="98"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag jo">
                        Иордания                                            ( الأردن )
                            </option>
                    <option value="65"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag iq">
                        Ирак                                            ( العراق )
                            </option>
                    <option value="45"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ir">
                        Иран                                            ( ایران )
                            </option>
                    <option value="58"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ie">
                        Ирландия                                            ( Éire )
                            </option>
                    <option value="22"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag es">
                        Испания                                            ( España )
                            </option>
                    <option value="16"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag it">
                        Италия                                            ( Italia )
                            </option>
                    <option value="5"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag kz">
                        Казахстан                                            ( Қазақстан )
                            </option>
                    <option value="61"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cm">
                        Камерун                                            ( Cameroon )
                            </option>
                    <option value="14"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ca">
                        Канада                                            ( Canada )
                            </option>
                    <option value="60"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cy">
                        Кипр                                            ( Κύπρος )
                            </option>
                    <option value="18"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag kg">
                        Киргизия                                            ( Kyrgyzstan )
                            </option>
                    <option value="30"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cn">
                        Китай                                            ( 中國 )
                            </option>
                    <option value="59"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cr">
                        Коста-Рика                                            ( Costa Rica )
                            </option>
                    <option value="96"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cu">
                        Куба                                            ( Cuba )
                            </option>
                    <option value="75"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag kw">
                        Кувейт                                            ( الكويت )
                            </option>
                    <option value="23"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag lv">
                        Латвия                                            ( Latvija )
                            </option>
                    <option value="76"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag lb">
                        Ливан                                            ( لبنان )
                            </option>
                    <option value="72"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ly">
                        Ливия                                            ( ليبيا )
                            </option>
                    <option value="17"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag lt">
                        Литва                                            ( Lietuva )
                            </option>
                    <option value="83"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag lu">
                        Люксембург                                            ( Luxembourg )
                            </option>
                    <option value="100"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag mk">
                        Македония                                            ( Македонија )
                            </option>
                    <option value="101"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag my">
                        Малайзия                                            ( Malaysia )
                            </option>
                    <option value="105"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag mt">
                        Мальта                                            ( Malta )
                            </option>
                    <option value="70"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ma">
                        Марокко                                            ( مغربي )
                            </option>
                    <option value="11"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag mx">
                        Мексика                                            ( México )
                            </option>
                    <option value="63"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag mz">
                        Мозамбик                                            ( Moçambique )
                            </option>
                    <option value="28"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag md">
                        Молдавия                                            ( Republica Moldova )
                            </option>
                    <option value="73"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag mc">
                        Монако                                            ( Monaco )
                            </option>
                    <option value="104"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag mn">
                        Монголия                                            ( Mongolia )
                            </option>
                    <option value="37"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag nz">
                        Новая Зеландия                                            ( New Zealand )
                            </option>
                    <option value="47"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag no">
                        Норвегия                                            ( Norge )
                            </option>
                    <option value="66"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag pk">
                        Пакистан                                            ( Pakistan )
                            </option>
                    <option value="102"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag pe">
                        Перу                                            ( Perú )
                            </option>
                    <option value="7"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag pl">
                        Польша                                            ( Polska )
                            </option>
                    <option value="49"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag pt">
                        Португалия                                            ( Portugal )
                            </option>
                    <option value="1"
            selected="selected"                    data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ru">
                        Россия                                            ( Russia )
                            </option>
                    <option value="38"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ro">
                        Румыния                                            ( România )
                            </option>
                    <option value="50"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag sv">
                        Сальвадор                                            ( Salvador )
                            </option>
                    <option value="94"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag sg">
                        Сингапур                                            ( Singapore )
                            </option>
                    <option value="87"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag sy">
                        Сирия                                            ( سوريا )
                            </option>
                    <option value="67"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag sk">
                        Словакия                                            ( Slovensko )
                            </option>
                    <option value="62"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag si">
                        Словения                                            ( Slovenija )
                            </option>
                    <option value="86"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag sr">
                        Суринам                                            ( Suriname )
                            </option>
                    <option value="2"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag us">
                        США                                            ( USA )
                            </option>
                    <option value="21"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag tj">
                        Таджикистан                                            ( Tajikistan )
                            </option>
                    <option value="88"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag th">
                        Таиланд                                            ( ประเทศไทย )
                            </option>
                    <option value="89"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag tn">
                        Тунис                                            ( تونس )
                            </option>
                    <option value="64"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag tm">
                        Туркмения                                            ( Turkmenistan )
                            </option>
                    <option value="32"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag tr">
                        Турция                                            ( Türkiye )
                            </option>
                    <option value="69"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ug">
                        Уганда                                            ( Uganda )
                            </option>
                    <option value="15"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag uz">
                        Узбекистан                                            ( Ўзбекистон )
                            </option>
                    <option value="3"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ua">
                        Украина                                            ( Україна )
                            </option>
                    <option value="41"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag fi">
                        Финляндия                                            ( Suomi )
                            </option>
                    <option value="12"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag fr">
                        Франция                                            ( France )
                            </option>
                    <option value="53"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag hr">
                        Хорватия                                            ( Hrvatska )
                            </option>
                    <option value="107"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag me">
                        Черногория                                            ( Црна Гора )
                            </option>
                    <option value="40"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cz">
                        Чехия                                            ( Česká republika )
                            </option>
                    <option value="93"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag cl">
                        Чили                                            ( Chile )
                            </option>
                    <option value="43"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ch">
                        Швейцария                                            ( Schweiz )
                            </option>
                    <option value="36"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag se">
                        Швеция                                            ( Sverige )
                            </option>
                    <option value="92"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ec">
                        Эквадор                                            ( Ecuador )
                            </option>
                    <option value="33"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag ee">
                        Эстония                                            ( Eesti )
                            </option>
                    <option value="35"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag kp">
                        Южная Корея                                            ( 대한민국 )
                            </option>
                    <option value="90"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag jm">
                        Ямайка                                            ( Jamaica )
                            </option>
                    <option value="6"
                                data-image="http://o3.vezetvsem.info/assets/images/msdropdown/icons/blank.gif"
                    data-imagecss="flag jp">
                        Япония                                            ( 日本 )
                            </option>
            </select>
</div>
<span class="x-required city-input">
    <input type="text"
            class="text1 x-ac-input x-ac-city x-ac-related x-open-up "
            placeholder="Используйте автоподбор"
            name="location[1][2][city]"
            value=""
            data-ac-item-id=""
            data-ac-related="460471187"
            data-ac-related-id="1677621051"
            data-ac-related-name="city_id"
            data-open-id="3">
</span>
<input type="text"
       class="text2 x-ac-input x-ac-street x-ac-related "
       placeholder="Улица"
       data-ac-related="1677621051" name="location[1][2][street]">
<input type="text"
       class="text3"
       placeholder="Дом"
       name="location[1][2][home]">
</div></div>