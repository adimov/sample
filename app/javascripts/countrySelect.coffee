setCustomSelect = ->
  $.widget "custom.coutryselect", $.ui.selectmenu,
    _renderItem: ( ul, item ) ->
      li = $( "<li>", text: item.label)
      if item.disabled
        li.addClass( "ui-state-disabled" )
      $("<span>",
        "class": "flag " + (item.element.attr( "data-image" )).toLowerCase()
      ).prependTo( li )
      return li.appendTo( ul )
getCoutries = ->
  request = $.ajax
    url: "http://localhost:4567/country"
    method: "GET"
  .done (response) ->
    $('.x-country').coutryselect(
      change: ( event, ui ) ->
        flag = ui.item.element.attr( "data-image" ).toLowerCase()
        $(this).parent().find('.x-country-flag').attr('class', "x-country-flag flag #{flag}")
    ).coutryselect("menuWidget")
    .addClass("ui-menu-countries")
    $('.x-country').find('option').remove()


    for country in response.data
      selected = "selected='selected'" if country.name is "Россия"

      $('.x-country').append("
        <option value='#{country.id}' data-image='#{country.iso}' #{selected}>
          #{country.name} (#{country.name_original})
        </option>
      ")
    $('.x-country').val('1')
setupCountrySelect = ->
  do setCustomSelect
  do getCoutries


module.exports = setupCountrySelect
