dateFrom = moment(new Date()).format('DD.MM.YYYY')
dateTill = moment(new Date()).format('DD.MM.YYYY')

setDatepicker = ->

  $('.x-datepicker').dateRangePicker
    autoClose: true
    showShortcuts: false
    format: 'DD.MM.YYYY'
    startDate: new Date()
    separator: " — "
    hoveringTooltip: false
    language: 'ru'
  .bind 'datepicker-change', (event,obj) ->
    dateFrom = obj.date1
    $("input[name='order[0][12][dates][date_from]']").val moment(dateFrom).format('DD.MM.YYYY')
    if obj.date2
      dateTill = obj.date2
      $("input[name='order[0][12][dates][date_to]']").val moment(dateTill).format('DD.MM.YYYY')


  $('#x-opendate').change ->
    state = $("#x-opendate").prop('checked')


    $('.x-datepicker').data('dateRangePicker').destroy()
    if dateTill > dateFrom
      nextDate = moment(dateTill).format('DD.MM.YYYY')
    else
      nextDate = moment(dateFrom).add(1,'days').format('DD.MM.YYYY')

    nextDateWithDash = if state then '' else ' — ' + nextDate

    if state
      $("input[name='order[0][12][dates][date_to]']").val('')
    else
      $("input[name='order[0][12][dates][date_to]']").val(nextDate)

    $('.x-datepicker').val moment(dateFrom).format('DD.MM.YYYY') + nextDateWithDash
    $('.x-datepicker').dateRangePicker
      autoClose: true
      showShortcuts: false
      format: 'DD.MM.YYYY'
      startDate: new Date()
      separator: " — "
      singleDate: state
      hoveringTooltip: false
      language: 'ru'



module.exports = setDatepicker



