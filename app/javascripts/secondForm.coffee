setSecondFormHeight = ->
  $('.second-form').css
    maxHeight: $('.offer').outerHeight()+$('.first-form').outerHeight()


setFromValidatiton = ->
  $('.second-form input, .second-form textarea').keyup ->
    $(this).removeClass('hasError')
  $('.x-datepicker').change ->
    $(this).removeClass('hasError')
  $('.x-datepicker').click ->
    $(this).removeClass('hasError')


validateSecondForm = ->
  valid = true
  if $('.order_title').val() is ""
    valid = false
    $('.order_title').addClass('hasError')
  if $("textarea[name='order[0][12][comment]']").val() is ""
    valid = false
    $("textarea[name='order[0][12][comment]']").addClass('hasError')
  return valid


setErrorTooltip = (name, text) ->
  if (name is "order[0][12][dates][date_from]") or (name is "order[0][12][dates][date_to]")
    $target = $('.x-datepicker').addClass('hasError')
  else
    $target = $("input[name='#{name}'], textarea[name='#{name}']").addClass('hasError')
  $tooltip = $target.next()
  $tooltip.css
    'max-width': $target.outerWidth()
    'left': $target.position().left+($target.outerWidth()/2)
    'transform': 'translateX(-50%)'
    'margin-left': '0.5%'
  $target.next().text(text)


setSecondFormSubmit = ->
  $("<div class='tooltip'></div>").insertAfter('.order_data input, .order_data textarea')
  $('.send').click ->
    if validateSecondForm()
      $.ajax
        url: "http://localhost:4567/order/add"
        data: $('.order_data').serialize()
        success: (data) ->
          data = JSON.parse(data)
          if data.success is 1
            console.log 'zaebis'
            $('.hasError').removeClass('hasError')
          else
            for name, text of data.errors
              setErrorTooltip(name,text)


        error: (data) ->
          alert('Кажется что-то пошло не так.')



setPlusMinusButtons = ->
  $('.plus').click ->
    $(this).addClass('hidden').parent().next().slideDown()
  $('.minus').click ->
    $(this).parent().slideUp().find('input').val('')
    $(this).parent().prev().find('.hidden').removeClass('hidden')




setupSecondForm = ->
  do setSecondFormHeight
  do setFromValidatiton
  do setSecondFormSubmit
  do setPlusMinusButtons


module.exports = setupSecondForm


