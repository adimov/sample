setAutocomplete = ->
  $(".x-autocomplete").change ->
    $(this).removeClass('selected')
  $(".x-autocomplete").autocomplete
    source: ( request, response ) ->
      county_id = this.element.parent().find('.x-country').val()
      city_id = this.element.parent().find('.x-city').data('city-id')
      path = this.element.data 'autocomplete'
      if path is 'suggest_city'
        data = { country_id: county_id, query: request.term }
      else
        data = { city_id: city_id, query: request.term }

      unless ( (path is 'suggest_street') and !city_id )
        $.ajax
          url: "http://localhost:4567/location/#{path}"
          dataType: "json"
          data: data
          success: ( data ) ->
            response $.map(data, (item) ->
                return {
                  label: item.name
                  value: item.name
                  id: item.id
                }
            )

    minLength: 2
    select: ( event, ui ) ->
      $element = $(event.target)
      if $element.data('autocomplete') is 'suggest_city'
        $element.data('city-id', ui.item.id)
      else
        $element.data('street-id', ui.item.id)

      $element.addClass('selected')





module.exports = setAutocomplete

