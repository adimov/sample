setErrorTooltip = ->
  $('.first-form input').each (i,e) ->
    $target = $(e)
    $target.next()
    .css
      'max-width': $target.outerWidth()
      'left': $target.position().left+($target.outerWidth()/2)
      'transform': 'translateX(-50%)'
      'margin-left': '0.5%'


validateEmail = (email) ->
    re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
    return re.test(email)

validateFirstForm = ->
  valid = true
  unless $('.first-form .x-name').val().length
    $('.first-form .x-name').addClass('hasError')
    valid = false
  else
    $('.first-form .x-name').removeClass('hasError')

  unless $('.first-form .x-phone').val().length is 18
    $('.first-form .x-phone').addClass('hasError')
    valid = false
  else
    $('.first-form .x-phone').removeClass('hasError')

  unless validateEmail($('.first-form .x-email').val())
    $('.first-form .x-email').addClass('hasError')
    valid = false
  else
    $('.first-form .x-email').removeClass('hasError')

   return valid



handleNextStep = ->
  $('body').addClass('transition-step-1')
  $('.offer').one "transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", ->
    $('body').removeClass('transition-step-1')
    .addClass('transition-step-2')
    setTimeout ->
      $('body').removeClass('transition-step-2')
      .addClass('transition-step-3')
    , 100

  $('.second-form').one "transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", ->
    $('body').removeClass('transition-step-3')
    .addClass('transition-step-4')

  $('body, html').stop().animate {scrollTop: 0}, '300'



setFirstFormSubmit = ->
  # $('.vv-next').click ->


  $('.first-form').on 'submit', (e) ->
    e.preventDefault()
    if validateFirstForm()
      $.ajax
        url: "http://localhost:4567/firstform"
        data: $('.first-form').serialize()
        success: ->
          do handleNextStep
      .done ->
        do handleNextStep
      .fail ->
        do handleNextStep




setFromValidatiton = ->

  $('.first-form input').keyup ->
    $(this).removeClass('hasError')

  $('.x-phone').mask '+0 (000) 000-00-00',
    onInvalid: (val, e, f, invalid, options) ->
      $('.x-phone').addClass('hasError')
    onComplete: ->
      $('.x-phone').removeClass('hasError')


setupFirstForm = ->
  do setErrorTooltip
  do setFirstFormSubmit
  do setFromValidatiton





module.exports = setupFirstForm

