require 'sinatra'
require 'json'
require 'sinatra/cross_origin'

configure do
  enable :cross_origin
end

get '/firstform' do
  cross_origin :allow_origin => '*',
    :allow_methods => [:get],
    :allow_credentials => false,
    :max_age => "60"

  return [200, {}, ['']]

end

get '/order/add' do
  cross_origin :allow_origin => '*',
    :allow_methods => [:get],
    :allow_credentials => false,
    :max_age => "60"
  if params['order']['0']['12']['title'] != "test"
    return [200, {}, ["{\"success\": 1}"]]
  else
    return [200, {}, [
      "{\"success\": 0,
        \"errors\":{
        \"location[0][1][street]\":\"Текст ошибки, все очень плохо\",
        \"order[0][12][comment]\": \"А эта ошибка даже хуже предыдущей, ваще кошмар\",
        \"order[0][12][dates][date_from]\": \"Еще и время, блядь, напутал\",
        \"location[0][1][home]\": \"Еще и время, блядь, напутал\"
      } }"]]
  end

end

get '/country' do
  cross_origin :allow_origin => '*',
    :allow_methods => [:get],
    :allow_credentials => false,
    :max_age => "60"

  content_type :json
  File.read('countries.json')
end


get '/location/suggest_city' do
  cross_origin :allow_origin => '*',
    :allow_methods => [:get],
    :allow_credentials => false,
    :max_age => "60"

  response = [{:id => 1, :name => 'Ростов-на-Дону'}, {:id => 2, :name => 'Москва'}].to_json
  response

end

get '/location/suggest_street' do
  cross_origin :allow_origin => '*',
    :allow_methods => [:get],
    :allow_credentials => false,
    :max_age => "60"

  response = [{:id => 1, :name => 'Садовая'}, {:id => 2, :name => 'Ленина'}].to_json
  response



end